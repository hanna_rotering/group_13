
## Name
AdPro Group Project

## Description
Our team participates in the Hackathon to analyze the energy mix of several countries. 
For that, the energy consumption of several countries is being analyzed in depth and their consumption is compared. Also, the CO2 emission is taken into consideration in our analysis.
The overall goal of this project is a step towards a greener transformation.

(No Badges and Visuals needed for this ReadMe File)

There are no installations necessary. 

The developed model can be used for any county of the dataset to have insights of their energy consumption and benchmark them with other countries. 

For further help, any of the authors can be contacted.

## Authors and acknowledgment
Benedikt Busch - 48211
Frederikke Bruhn Sørensen - 49721
George Mcguigan - 48685
Hanna Rotering - 48704

## License
The license used here is GNU GENERAL PUBLIC LICENSE.


