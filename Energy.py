import os
import numpy as np
import pandas as pd
import seaborn as sns
import black
from pathlib import Path
import urllib
import requests
from functools import reduce
import matplotlib.pyplot as plt
import csv
import os.path
from os import path
import plotly.express as px
import warnings
from statsmodels.tsa.arima_model import ARIMA
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()


class EnergyMix:
    """A class to download and perform analysis on the energy data from the given url

    Attributes
    -----------
    download_successful: boolean
    Boolean to indicate whether or not data has been downloaded that the methods can work with

    Methods
    -----------

    __init__: Init method
    Class constructor to inizialize the attributes of the class.

    download_data: Method 1
    Downloads required data from given url, saves it in download folder
    and creates panda dataframe.

    compare_gdp(some_country: str, some_list: list) -> pd.Dataframe
        Allows for the comparison of gdp by country over years

    gapminder(year: int)
        Shows a gampinder style chart of gdp compared to energy consumption per country for a chosen year

    """

    df = ""
    download_successful = False
    url = "https://nyc3.digitaloceanspaces.com/owid-public/data/energy/owid-energy-data.csv"

    def __init__(self):
        """To represent instance of the class and initialze variables for further methods"""

        self.df = self.download_data(self.url)

    def download_data(
        self, file_link: str, output_file: str = "energymix.csv"
    ) -> pd.DataFrame:

        """Download relevant dataset for project

        Parameters
        ------------
        file_link: str
            A string which contains the url with the data to be downloaded
        output_file: str
            A string containing the name of the output file. The default value is 'file.csv'
            at the location you are running the function.

        Returns
        ------------
        Dataframe

        Examples
        ------------
        download_file("https://archive.ics.uci.edu/ml/machine-learning-databases/00320/student.zip",
                  output_file='student.zip')"""

        # url = "https://nyc3.digitaloceanspaces.com/owid-public/data/energy/owid-energy-data.csv"

        r = requests.get(self.url, allow_redirects=True)
        filename = "energymix.csv"
        f = ""

        try:
            if not path.exists("downloads"):
                os.mkdir("downloads")

            elif path.exists(os.path.join("downloads", filename)):
                print("File already downloaded")

            else:
                with open(os.path.join("downloads", filename), "wb") as f:
                    f.write(r.content)
                    print("File successfully downloaded")

        except:
            print("There was an error downloading / writing the file")

        self.df = pd.read_csv(os.path.join("downloads", filename))
        self.df = self.df[self.df["year"] > 1970].reset_index(drop=True)

        # First we extract the consumption-like columns from the original dataframe into a new dataframe

        consumption_df = self.df.filter(like="_consumption")

        # Then, we drop these exact columns so that we don't have doubles when we merge the "pure consumption" into the big DF

        self.df = self.df[
            self.df.columns.drop(list(self.df.filter(regex="_consumption")))
        ]

        # only pure energy sources
        pure = consumption_df[
            [
                "primary_energy_consumption",
                "biofuel_consumption",
                "coal_consumption",
                "gas_consumption",
                "hydro_consumption",
                "nuclear_consumption",
                "oil_consumption",
                "solar_consumption",
                "wind_consumption",
            ]
        ]
        pure.dropna(inplace=True)

        # convert from twh to kwh
        pure.iloc[:] *= 1e9

        self.df = pd.merge(self.df, pure, left_index=True, right_index=True)

        # calculate gco2 per kwh
        co2_emission = (
            self.df["biofuel_consumption"] * 1450
            + self.df["coal_consumption"] * 1000
            + self.df["gas_consumption"] * 455
            + self.df["hydro_consumption"] * 90
            + self.df["nuclear_consumption"] * 5.5
            + self.df["oil_consumption"] * 1200
            + self.df["solar_consumption"] * 53
            + self.df["wind_consumption"] * 14
        )

        # add column to dataframe
        self.df["emissions"] = co2_emission

        ## convert gco2eq/kw to tones
        self.df["CO2 emissions (in tonnes)"] = self.df["emissions"] / 1e6

        self.df["year"] = pd.to_datetime(self.df["year"], format="%Y")
        self.df["year"] = self.df["year"].apply(lambda x: x.strftime("%Y"))
        self.df = self.df.set_index("year")

        self.download_successful = True

        return self.df

    # Method 3

    """ Plots area chart of consumption for countries in the dataset 
        
        Parameters
        -------------
        some_country: str
            A string which contains the name of the country for which energy consumption data should be retrieved
        some_list: list
            A list of countries which allows for comparison of several countries' energy consumption overthe years in a side-by-side fashion
            
        Returns
        -------------
        plt : area chart 
        area chart of energy consumption for chosen countries 
            
        Examples
        -------------
        consumption plot("", "Hungary, Germany")
        """

    def consumption_plot(self, country: str, normalize: bool):
        if not self.df["country"].str.contains(country).any():
            raise KeyError(f"Selected country {country} does not exist in dataset.")
        else:
            if not self.df.index[0] == 0:
                self.df.reset_index(inplace=True)

            new_df = self.df[self.df["country"] == country][
                [
                    "year",
                    "country",
                    "biofuel_consumption",
                    "coal_consumption",
                    "gas_consumption",
                    "hydro_consumption",
                    "nuclear_consumption",
                    "oil_consumption",
                    "solar_consumption",
                    "wind_consumption",
                ]
            ]

            if normalize == True:
                consumption_p_df = new_df.iloc[:, 2:].div(
                    new_df.sum(numeric_only=True, axis=1), axis=0
                )
                consumption_p_df["year"] = new_df["year"]
                consumption_p_df["country"] = new_df["country"]

                consumption_p_df.plot.area(
                    x="year",
                    stacked=True,
                    figsize=(15, 10),
                    title=("Relative Energy Consumption per Country"),
                    grid=True,
                    xlabel="Year",
                    ylabel="Energy Consumption",
                )

            else:
                new_df = self.df[self.df["country"] == country][
                    [
                        "year",
                        "country",
                        "biofuel_consumption",
                        "coal_consumption",
                        "gas_consumption",
                        "hydro_consumption",
                        "nuclear_consumption",
                        "oil_consumption",
                        "solar_consumption",
                        "wind_consumption",
                    ]
                ]
                new_df.plot.area(
                    x="year",
                    stacked=True,
                    figsize=(15, 10),
                    title=("Energy Consumption per Country"),
                    grid=True,
                    xlabel="Year",
                    ylabel="Energy Consumption",
                )

    # Method 4

    def compare_consumption(self, countries: list):
        """Compares the energy consumption of chosen countries

        Parameters
        -------------
        some_list: list
            A list of countries which allows for comparison of several countries' energy consumption over the years in a side-by-side fashion

        Returns
        -------------
        plt : bar chart
        bar chart of energy consumption for chosen countries

        Examples
        -------------
        compare_consumption("", "Hungary, Germany")
        """
        consumption_df = self.df.filter(like="_consumption")
        country_df = self.df[["country"]]
        df1 = pd.merge(country_df, consumption_df, left_index=True, right_index=True)
        df2 = self.df.groupby("country").mean()
        df2["total_consumption"] = df2.iloc[:, :].sum(axis=1)
        df_all = df2.reset_index()

        country_list = []
        energy_list = []
        emission_list = []

        for country in countries:
            energy = df_all[df_all["country"] == country]["total_consumption"].values[0]
            emission = df_all[df_all["country"] == country][
                "CO2 emissions (in tonnes)"
            ].values[0]
            country_list.append(country)
            emission_list.append(emission)
            energy_list.append(energy)

        x = country_list
        y1 = energy_list
        y2 = emission_list

        fig, ax = plt.subplots(figsize=(20, 10))

        ax2 = ax.twinx()

        ax.bar(x, y1, color="orange", width=0.2)
        ax2.plot(x, y2, "--")

        ax.set_xlabel("country")
        ax.set_ylabel("consumption")
        ax2.set_ylabel("co2 emissions")
        ax.legend(["energy consumption"], loc="upper left")
        ax2.legend(["co2 emissions"], loc="upper right")

        plt.tight_layout()
        plt.show()

    def compare_gdp(self, some_country: str = "", some_list: list = []) -> pd.DataFrame:

        """Allows for the comparison of gdp by country over years

        Parameters
        -------------
        some_country: str
            A string which contains the name of the country for which GDP data should be retrieved
        some_list: list
            A list of countries which allows for comparison of several countries' GDP overthe years in a side-by-side fashion

        Returns
        -------------
        df_new: Pandas Dataframe
            The dataframe containing the GDP comparison that the user will inspect

        Examples
        -------------
        compare_gdp("", ["Hungary, Germany"])
        """
        if self.download_successful == True:
            if not self.df.index[0] == 0:
                self.df.reset_index(inplace=True)

            df_reduced = self.df[["country", "year", "gdp"]]
            df_only_some_country = self.df[self.df["country"] == some_country]

            if not some_country == "":
                df_new = df_reduced[df_reduced["country"] == some_country][
                    "_consumption"
                ]
                # return df_new

            else:
                df_list = []
                for element in some_list:
                    df_list.append(df_reduced[df_reduced["country"] == element])
                df_new = reduce(lambda df1, df2: pd.merge(df1, df2, on="year"), df_list)
                df_new = df_new.set_index("year")
                df_new.filter(like="gdp")
                fig, ax = plt.subplots()
                df_new.plot(ax=ax)
                ax.legend(some_list)

                # return df_new
        else:
            print(
                "The data has not yet been downloaded. Please run class_name.download_data(url) first."
            )

    def gapminder(self, year: int):

        """Shows a gampinder style chart of gdp compared to energy consumption per country for a chosen year

        Parameters
        -------------
        year: int
            The year for which the data on energy consumption should be retrieved.


        Returns
        -------------
        Nothing

        Examples
        -------------
        gapminder(2014)
        """

        if not type(year) == int:
            raise TypeError("The input is not an integer")
        else:
            if self.download_successful == True:
                if not self.df.index[0] == 0:
                    self.df.reset_index(inplace=True)

                gap_df = self.df[self.df["year"].astype(int) == year][
                    [
                        "year",
                        "gdp",
                        "country",
                        "population",
                        "primary_energy_consumption",
                    ]
                ]
                gap_df_new = gap_df
                gap_df_new = gap_df[gap_df["country"] != "World"].copy()
                gap_df_new["population"].fillna(0, inplace=True)

                fig = px.scatter(
                    x=gap_df_new["gdp"],
                    y=gap_df_new["primary_energy_consumption"],
                    size=gap_df_new["population"],
                    color=gap_df_new["country"],
                    hover_name=gap_df_new["country"],
                    log_x=True,
                    log_y=True,
                    size_max=60,
                    labels={
                        "gdp": "GDP in Currency",
                        "primary_energy_consumption": "Energy Consumption in X",
                    },
                    title="GDP Compared to Energy Consumption per Country",
                )
                fig.show()

            else:
                print(
                    "The data has not yet been downloaded. Please run class_name.download_data(url) first."
                )

    def emission_consumption(self, year: int):

        """Shows a gampinder style chart of emission compared to energy consumption per country for a chosen year

        Parameters
        -------------
        year: int
            The year for which the data on energy and emission consumption should be retrieved.


        Returns
        -------------
        Nothing

        Examples
        -------------
        emission_consumption(2016)
        """

        if not type(year) == int:
            raise TypeError("The input is not an integer")

        else:
            if not self.df.index[0] == 0:
                self.df.reset_index(inplace=True)

            emission_df = self.df[self.df["year"].astype(int) == year][
                [
                    "year",
                    "emissions",
                    "country",
                    "population",
                    "primary_energy_consumption",
                ]
            ]
            emission_consumption = emission_df[emission_df["country"] != "World"].copy()
            emission_consumption["population"].fillna(0, inplace=True)

            fig = px.scatter(
                emission_consumption,
                x="emissions",
                y="primary_energy_consumption",
                size="population",
                color="country",
                hover_name="country",
                log_x=True,
                log_y=True,
                size_max=60,
                labels={
                    "emissions": "Total C02 emissions",
                    "primary_energy_consumption": "Energy Consumption",
                },
                title="Emission Consumption per Country taking into account Population",
            )
            fig.show()

    def list_method(self, unique: bool) -> pd.DataFrame:
        """Allows to output a list of the available countries in the data set

        Parameters
        -------------
        df: DataFrame
            A DataFrame which contains all the data collected from the energymix data set


        Returns
        -------------
        list_of_countries: Pandas Dataframe
            A list of available countries in the data set

        Examples
        -------------
        list_method(dataframe)

        """

        if self.download_successful == True:

            if unique == True:
                list_of_countries = self.df["country"].unique()
                return list_of_countries

            else:
                list_of_countries = self.df["country"]
                return list_of_countries

        else:
            print(
                "The data has not yet been downloaded. Please run class_name.download_data(url) first."
            )

    def arima(self, country: str, prediction_period: int):
        """
        Creates and plots a prediction model based on the ARIMA framework to predict both consumptions and emissions

        Parameters
        -------------
        country: str
            A string with the country for which the ARIMA analysis should be performed

        prediction_period: int
            An integer with the number of prediction periods following the last year of data


        Returns
        -------------
        Nothing

        Examples
        -------------
        arima("Germany", 10)

        """

        if not type(prediction_period) == int:
            raise TypeError("The input is not an integer")

        elif prediction_period < 2:
            raise ValueError("The prediction period must be at least 2")

        else:

            emissions_arima_df = self.df[self.df["country"] == country]
            emissions_arima_df = emissions_arima_df[["emissions"]]
            emissions_df_log = np.log(emissions_arima_df)
            emissions_df_log_shift = emissions_df_log - emissions_df_log.shift()
            emissions_df_log_shift.dropna(inplace=True)

            emissions_model = ARIMA(emissions_df_log, order=(2, 1, 2))
            emissions_results = emissions_model.fit(disp=-1)

            plt.rcParams["figure.figsize"] = (20, 3)
            emissions_results.plot_predict(1, 49 + prediction_period)
            plt.ylabel("emissions")

            consumption_arima_df = self.df[self.df["country"] == country]
            consumption_arima_df = consumption_arima_df[["primary_energy_consumption"]]
            consumption_df_log = np.log(consumption_arima_df)
            consumption_df_log_shift = consumption_df_log - consumption_df_log.shift()
            consumption_df_log_shift.dropna(inplace=True)

            consumption_model = ARIMA(consumption_df_log, order=(2, 1, 2))
            consumption_results = consumption_model.fit(disp=-1)

            plt.rcParams["figure.figsize"] = (20, 3)
            consumption_results.plot_predict(1, 49 + prediction_period)
            plt.ylabel("consumption")
