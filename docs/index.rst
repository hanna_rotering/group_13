.. Fancy Group Project documentation master file, created by
   sphinx-quickstart on Wed Mar 16 15:39:49 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Fancy Group Project's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents: 
   
   _apidocs/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
